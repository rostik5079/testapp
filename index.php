<?php
declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

$request_uri = $_SERVER['REQUEST_URI'];

$routes = require_once __DIR__ . '/src/routes/routes.php';

if (isset($routes[$request_uri])) {
    [$controllerClass, $methodName, $dependencies] = $routes[$request_uri];

    $resolvedDependencies = [];
    foreach ($dependencies as $dependencyName => $dependencyClass) {
        $resolvedDependencies[$dependencyName] = new $dependencyClass();
    }

    $controller = new $controllerClass(...array_values($resolvedDependencies));

    $controller->$methodName();
} else {
    http_response_code(404);
    echo 'Not Found 404';
}
