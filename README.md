# TestApp

## Docker
- Create .env from .env.dist
- Up and build docker container `docker-compose up -d --build`
- Enter to php container `docker exec -it php_app bash`
- Composer `composer install`
- Make user table `php userTableCreation.php`

You can open project: http://localhost:81/register
