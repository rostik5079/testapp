<?php
declare(strict_types=1);

namespace App\Src\Services\Validation;

class Validator
{
    public function validateLoginData(string $email, string $password): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false
            && strlen($password) >= 6 && strlen($password) <= 50;
    }

    public function validateRegistrationData(string $firstName, string $lastName, string $email, string $mobile, string $password): bool
    {
        return strlen($firstName) <= 50
            && strlen($lastName) <= 50
            && strlen($email) <= 50
            && strlen($mobile) <= 50
            && strlen($password) <= 50;
    }
}
