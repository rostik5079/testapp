<?php
declare(strict_types=1);

namespace App\Src\Services;

use App\Src\Models\User;
use mysqli;

class SaveUserToDb
{
    public function saveToDatabase(User $user, mysqli $db): void
    {
        $db->set_charset("utf8mb4");
        $stmt = $db->prepare("INSERT INTO users (first_name, last_name, email, mobile, password) VALUES (?, ?, ?, ?, ?)");
        if ($stmt === false) {
            die("Ошибка подготовки запроса: " . $db->error);
        }

        $firstName = $user->getFirstName();
        $lastName = $user->getLastName();
        $email = $user->getEmail();
        $mobile = $user->getMobile();
        $password = $user->getPassword();
        $stmt->bind_param("sssss", $firstName, $lastName, $email, $mobile, $password);

        if ($stmt->execute() === false) {
            die("Ошибка выполнения запроса: " . $stmt->error);
        }

        $stmt->close();
    }
}
