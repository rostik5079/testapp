<?php
declare(strict_types=1);

namespace App\Src\Services;

use App\Src\Models\User;
use mysqli;

class GetUserByEmail
{
    public function getUserByEmail(string $email, mysqli $db): ?User
    {
        $stmt = $db->prepare("SELECT * FROM users WHERE email = ?");
        if ($stmt === false) {
            die("Ошибка подготовки запроса: " . $db->error);
        }

        $stmt->bind_param("s", $email);

        if ($stmt->execute() === false) {
            die("Ошибка выполнения запроса: " . $stmt->error);
        }

        $result = $stmt->get_result();

        if ($result->num_rows === 0) {
            return null;
        }

        $userData = $result->fetch_assoc();
        if ($userData) {
            $user = new User(
                $userData['firstName'] ?? '',
                $userData['lastName'] ?? '',
                $userData['email'] ?? '',
                $userData['mobile'] ?? '',
                $userData['password'] ?? ''
            );
        } else {
            $user = null;
        }

        $stmt->close();

        return $user;
    }
}
