<?php
declare(strict_types=1);

use App\Src\Controllers\AuthController;
use App\Src\Controllers\DashboardController;
use App\Src\Controllers\RegistrationController;
use App\Src\Services\GetUserByEmail;
use App\Src\Services\SaveUserToDb;
use App\Src\Services\Validation\Validator;

return [
    '/register' => [
            RegistrationController::class,
        'register',
        [
            'saveUserToDb' => SaveUserToDb::class,
            'validator' => Validator::class
        ]
    ],
    '/login' => [
        AuthController::class,
        'login',
        [
            'getUserByEmail' => GetUserByEmail::class,
            'validator' => Validator::class
        ]
    ],
    '/dashboard' => [DashboardController::class, 'index', []],
];
