<?php

declare(strict_types=1);

namespace App\Src\Models;

class User {
    public function __construct(
        private string $firstName,
        private string $lastName,
        private string $email,
        private string $mobile,
        private string $password,
        private int $id = 0
    ) {}

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getMobile(): string
    {
        return $this->mobile;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
