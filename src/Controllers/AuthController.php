<?php
declare(strict_types=1);

namespace App\Src\Controllers;

use App\Src\Services\GetUserByEmail;
use App\Src\Services\Validation\Validator;

class AuthController extends MainController
{
    public function __construct(
        private GetUserByEmail $getUserByEmail,
        private Validator $validator
    )
    {
        parent::__construct();
    }

    public function login(): void
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $email = $_POST['email'] ?? '';
            $password = $_POST['password'] ?? '';

            if ($this->validator->validateLoginData($email, $password)) {
                $user = $this->getUserByEmail->getUserByEmail($email, $this->getConnection());

                if ($user && password_verify($password, $user->getPassword())) {
                    $_SESSION['user_id'] = $user->getId();

                    header('Location: /dashboard');
                    exit;
                } else {
                    echo 'Ошибка: Неверные учетные данные';
                }
            } else {
                echo 'Ошибка: Некорректные данные';
            }
        }

        include_once(__DIR__ . '/../views/auth/login.php');
    }


}
