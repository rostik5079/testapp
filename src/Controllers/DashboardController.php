<?php
declare(strict_types=1);

namespace App\Src\Controllers;

class DashboardController extends MainController
{
    public function index(): void
    {
        if (!isset($_SESSION['user_id'])) {
            header('Location: /login');
            exit;
        }

        include_once(__DIR__ . '/../views/main.php');
    }
}
