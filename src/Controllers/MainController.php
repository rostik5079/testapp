<?php
declare(strict_types=1);

namespace App\Src\Controllers;

use mysqli;

class MainController
{
    private ?mysqli $connection = null;

    public function __construct()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
    }

    public function openConnection(): void
    {
        if ($this->connection === null) {
            $this->connection = new mysqli(
                getenv('DB_HOST'),
                getenv('DB_USERNAME'),
                getenv('DB_PASSWORD'),
                getenv('DB_DATABASE')
            );

            if ($this->connection->connect_error) {
                die("Ошибка подключения к базе данных: " . $this->connection->connect_error);
            }
        }
    }

    public function getConnection(): mysqli
    {
        $this->openConnection();
        return $this->connection;
    }
}
