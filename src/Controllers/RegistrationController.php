<?php

declare(strict_types=1);

namespace App\Src\Controllers;

use App\Src\Models\User;
use App\Src\Services\SaveUserToDb;
use App\Src\Services\Validation\Validator;

class RegistrationController extends MainController
{
    public function __construct(
        private SaveUserToDb $saveUserToDb,
        private Validator    $validator
    )
    {
        parent::__construct();
    }

    public function register(): void
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $firstName = $_POST['firstName'];
            $lastName = $_POST['lastName'];
            $email = $_POST['email'];
            $mobile = $_POST['mobile'];
            $password = $_POST['password'];

            if ($this->validator->validateRegistrationData($firstName, $lastName, $email, $mobile, $password)) {
                $hashedPassword = password_hash($password, PASSWORD_BCRYPT);
                $user = new User($firstName, $lastName, $email, $mobile, $hashedPassword);
                $this->saveUserToDb->saveToDatabase($user, $this->getConnection());

                header('Location: /login');
                exit;
            } else {
                echo 'Ошибка: Превышена максимальная длина данных';
            }
        }
        include_once(__DIR__ . '/../views/auth/register.php');
    }
}
